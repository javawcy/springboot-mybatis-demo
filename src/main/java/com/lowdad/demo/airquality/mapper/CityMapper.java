package com.lowdad.demo.airquality.mapper;

import com.lowdad.demo.airquality.forms.AddCityParams;
import com.lowdad.demo.airquality.forms.EditCityParams;
import com.lowdad.demo.airquality.model.CityModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lowdad
 * 2019-05-16
 **/
@Mapper
public interface CityMapper {

    int insert(AddCityParams addCityParams);

    List<CityModel> getList();

    CityModel getByCityName(AddCityParams addCityParams);

    CityModel getById(Long id);

    int update(EditCityParams editCityParams);

    int delete(Long id);
}
