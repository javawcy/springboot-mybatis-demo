package com.lowdad.demo.airquality.forms;

/**
 * @author lowdad
 * 2019-05-16
 **/
public class AddCityParams {

    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

}


