package com.lowdad.demo.airquality.forms;

/**
 * @author lowdad
 * 2019-05-16
 **/
public class DeleteCityParams {
    private Long cityId;

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
}
