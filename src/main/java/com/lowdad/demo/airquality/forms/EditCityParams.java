package com.lowdad.demo.airquality.forms;

/**
 * @author lowdad
 * 2019-05-16
 **/
public class EditCityParams {

    private String cityName;
    private Long cityId;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }
}
