package com.lowdad.demo.airquality.service;

import com.lowdad.demo.airquality.VO.CityReturn;
import com.lowdad.demo.airquality.forms.AddCityParams;
import com.lowdad.demo.airquality.forms.DeleteCityParams;
import com.lowdad.demo.airquality.forms.EditCityParams;

import java.util.List;

/**
 * @author lowdad
 * 2019-05-16
 **/
public interface CityService {

    String add(AddCityParams addCityParams);

    List<CityReturn> getCityList();

    String edit(EditCityParams editCityParams);

    String delete(DeleteCityParams deleteCityParams);
}
