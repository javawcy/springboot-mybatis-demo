package com.lowdad.demo.airquality.service.impl;

import com.lowdad.demo.airquality.VO.CityReturn;
import com.lowdad.demo.airquality.forms.AddCityParams;
import com.lowdad.demo.airquality.forms.DeleteCityParams;
import com.lowdad.demo.airquality.forms.EditCityParams;
import com.lowdad.demo.airquality.mapper.CityMapper;
import com.lowdad.demo.airquality.model.CityModel;
import com.lowdad.demo.airquality.service.CityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lowdad
 * 2019-05-16
 **/
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;

    @Override
    public String add(AddCityParams addCityParams) {

        //Step1: 首先要确定参数正确
        if (addCityParams.getCityName() == null) {
            return "fail,cityName can not be null";
        }

        //Step2: 先查询数据库，保证无重复
        CityModel model = cityMapper.getByCityName(addCityParams);
        if (model != null) {
            return "fail,cityName already exist";
        }

        //Step3: 如果不为空插入数据库
        int result = cityMapper.insert(addCityParams);

        if (result != 1) {
            return "fail,server error";
        }

        //Step4: 如果成功
        return "success";
    }

    @Override
    public List<CityReturn> getCityList() {
        //Step1:去数据库查询列表
        List<CityModel> list = cityMapper.getList();

        //Step2: 准备一个返回值集合
        List<CityReturn> returns = new ArrayList<>();

        //Step3: 遍历转化返回值
        for (CityModel cityModel : list) {
            CityReturn cityReturn = new CityReturn();
            BeanUtils.copyProperties(cityModel,cityReturn);

            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cityReturn.setCreatedAt(sf.format(cityModel.getCreatedAt()));
            cityReturn.setUpdatedAt(sf.format(cityModel.getUpdatedAt()));

            returns.add(cityReturn);
        }
        return returns;
    }

    @Override
    public String edit(EditCityParams editCityParams) {

        //Step1: 首先要确定参数正确
        if (editCityParams.getCityName() == null) {
            return "fail,cityName can not be null";
        }

        if (editCityParams.getCityId() == null) {
            return "fail,cityId can not be null";
        }

        //Step2: 找出要修改的那一条
        CityModel model = cityMapper.getById(editCityParams.getCityId());
        if (model == null) {
            return "fail,city not found";
        }

        //Step3: 如果存在就更新
        int result = cityMapper.update(editCityParams);

        if (result != 1) {
            return "fail,server error";
        }

        return "success";
    }

    @Override
    public String delete(DeleteCityParams deleteCityParams) {

        //Step1: 首先要确定参数正确
        if (deleteCityParams.getCityId() == null) {
            return "fail,cityId can not be null";
        }

        //Step2: 找出要修改的那一条
        CityModel model = cityMapper.getById(deleteCityParams.getCityId());
        if (model == null) {
            return "fail,city not found";
        }

        //Step3: 删除
        int result = cityMapper.delete(deleteCityParams.getCityId());

        if (result != 1) {
            return "fail,server error";
        }

        return "success";
    }

}
