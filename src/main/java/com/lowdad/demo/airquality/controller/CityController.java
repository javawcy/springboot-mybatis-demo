package com.lowdad.demo.airquality.controller;

import com.lowdad.demo.airquality.VO.CityReturn;
import com.lowdad.demo.airquality.forms.AddCityParams;
import com.lowdad.demo.airquality.forms.DeleteCityParams;
import com.lowdad.demo.airquality.forms.EditCityParams;
import com.lowdad.demo.airquality.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    @PostMapping("/add")
    public String addCity(
            AddCityParams addCityParams
    ) {
        return cityService.add(addCityParams);
    }

    @PostMapping("/edit")
    public String editCity(
            EditCityParams editCityParams
    ) {
        return cityService.edit(editCityParams);
    }

    @GetMapping("/cities")
    public List<CityReturn> getCityList() {
        return cityService.getCityList();
    }

    @PostMapping("/delete")
    public String delete(
            DeleteCityParams deleteCityParams
    ) {
        return cityService.delete(deleteCityParams);
    }

}
